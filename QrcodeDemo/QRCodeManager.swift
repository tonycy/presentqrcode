//
//  QRCodeManager.swift
//  QrcodeDemo
//
//  Created by chia on 2020/5/13.
//  Copyright © 2020 chia. All rights reserved.
//

import UIKit

class QRCodeManager: NSObject {
  
  static func generateQRcode_ciImage(_ text: String) -> CIImage? {
    let data = Data(text.utf8)
//    let data = text.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
    let filter = CIFilter(name: "CIQRCodeGenerator")
    filter?.setValue(data as Any?, forKey: "inputMessage")
    //filter?.setValue("H", forKey: "inputCorrectionLevel")
    return filter?.outputImage
  }
  
  static func displayQRCodeImage(qrCodeImageView: UIImageView, qrcodeImage: CIImage) {
    // transform size to fit imageView
    let scaleX = qrCodeImageView.frame.size.width / qrcodeImage.extent.size.width
    let scaleY = qrCodeImageView.frame.size.height / qrcodeImage.extent.size.height
    let transformedImage = qrcodeImage.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
    // make image transpant
    let colorFilter: CIFilter = CIFilter(name: "CIFalseColor")!
    colorFilter.setDefaults()
    colorFilter.setValue(transformedImage, forKey: "inputImage")
    let transparentBG: CIColor = CIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
    let foreColor: CIColor = CIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1)
    colorFilter.setValue(foreColor, forKey: "inputColor0")
    colorFilter.setValue(transparentBG, forKey: "inputColor1")
    let transpantImage = colorFilter.outputImage!
    qrCodeImageView.image = UIImage(ciImage: transpantImage)
  }
}
