//
//  QRCodeViewController.swift
//  QrcodeDemo
//
//  Created by chia on 2020/5/12.
//  Copyright © 2020 chia. All rights reserved.
//

import UIKit

class QRCodeViewController: UIViewController {
  
  @IBOutlet weak var imageView: UIImageView!
  var qrStringStr:String?
  var qrNumberStr:String?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    presentQRCode()

  }
  
  private func presentQRCode(){
    let string = "My Text:\n\(qrStringStr ?? "")\n\nNumber:\n\(qrNumberStr ?? "")"
    if let image = QRCodeManager.generateQRcode_ciImage(string) {
      QRCodeManager.displayQRCodeImage(qrCodeImageView:imageView, qrcodeImage: image)
    }
  }
  
  
}
