//
//  ViewController.swift
//  QrcodeDemo
//
//  Created by chia on 2020/5/12.
//  Copyright © 2020 chia. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  @IBOutlet weak var inputString_tf: UITextField!
  @IBOutlet weak var inputNumber_tf: UITextField!
  var tfield:UITextField?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    addGesture()
  }

  @IBAction func generateTapped(_ sender: UIButton) {
    performSegue(withIdentifier: "qrsegue", sender: nil)
  }
}

extension ViewController {
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    guard let vc = segue.destination as? QRCodeViewController else { return }
    vc.qrNumberStr = inputNumber_tf.text
    vc.qrStringStr = inputString_tf.text
  }
}

extension ViewController: UIGestureRecognizerDelegate {
  
  func addGesture(){
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTap(recognizer:)))
    tapGesture.delegate = self
    self.view?.gestureRecognizers = [tapGesture]
  }
  
  @objc func didTap(recognizer: UIPinchGestureRecognizer) {
    tfield?.resignFirstResponder()
  }
}

extension ViewController: UITextFieldDelegate {
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    tfield = textField
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    tfield = textField
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    return true
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
  }
}


